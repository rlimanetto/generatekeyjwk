package generateKeyJWK.exception;

public class ErroInesperadoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ErroInesperadoException() {
		super();
	}

	public ErroInesperadoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ErroInesperadoException(String message, Throwable cause) {
		super(message, cause);
	}

	public ErroInesperadoException(String message) {
		super(message);
	}

	public ErroInesperadoException(Throwable cause) {
		super(cause);
	}

}
