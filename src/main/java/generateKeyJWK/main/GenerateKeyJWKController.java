/**
 * 
 */
package generateKeyJWK.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import generateKeyJWK.request.GenerateKeyJWKRequest;
import generateKeyJWK.request.GenerateTokenRequest;
import generateKeyJWK.response.GenerateKeyJWKResponse;
import generateKeyJWK.response.GenerateTokenResponse;
import generateKeyJWK.service.GenerateKeyJWKService;

@Controller
@RequestMapping("/rest/v1/generatekey")
public class GenerateKeyJWKController {

	@Autowired
	private GenerateKeyJWKService generateKeyJWKService;

	@CrossOrigin(methods = RequestMethod.POST, allowCredentials = "true")
	@RequestMapping(value = "/gerarChave", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenerateKeyJWKResponse	gerarChave(	@RequestBody GenerateKeyJWKRequest jwkRequest) {
		return generateKeyJWKService.gerarChave(jwkRequest);
	}

	@CrossOrigin(methods = RequestMethod.GET, allowCredentials = "true")
	@RequestMapping(value = "/status", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GenerateKeyJWKResponse getStatus() {
		return generateKeyJWKService.getStatus();
	}
	
	@CrossOrigin(methods = RequestMethod.POST, allowCredentials = "true")
	@RequestMapping(value = "/gerarToken", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GenerateTokenResponse gerarToken(	@RequestBody GenerateTokenRequest generateTokenRequest) {
		return generateKeyJWKService.generateToken(generateTokenRequest);
	}
}
