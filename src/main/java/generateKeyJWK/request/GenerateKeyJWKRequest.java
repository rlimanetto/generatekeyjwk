package generateKeyJWK.request;

import org.codehaus.jackson.annotate.JsonAutoDetect;

@JsonAutoDetect
public class GenerateKeyJWKRequest {

	private String tipoChave;
	private Integer tamanhoChave;
	private String idChave;

	/**
	 * @return tipoChave
	 */
	public String getTipoChave() {
		return tipoChave;
	}

	/**
	 * @param tipoChave
	 */
	public void setTipoChave(String tipoChave) {
		this.tipoChave = tipoChave;
	}

	/**
	 * @return tamanhoChave
	 */
	public Integer getTamanhoChave() {
		return tamanhoChave;
	}

	/**
	 * @param tamanhoChave
	 */
	public void setTamanhoChave(Integer tamanhoChave) {
		this.tamanhoChave = tamanhoChave;
	}

	/**
	 * @return idChave
	 */
	public String getIdChave() {
		return idChave;
	}

	/**
	 * @param idChave
	 */
	public void setIdChave(String idChave) {
		this.idChave = idChave;
	}

}
