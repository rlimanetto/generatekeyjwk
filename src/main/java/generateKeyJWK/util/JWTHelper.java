package generateKeyJWK.util;

import java.util.Map;

import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.lang.JoseException;

public class JWTHelper
{
	private PublicJsonWebKey jsonWebKey;
	private String creatorToken;
	private String receiverToken;
	private Integer expirationMinutes;
	private String subject;

	public void setCreatorToken(String creatorToken)
	{
		this.creatorToken = creatorToken;
	}

	public void setReceiverToken(String receiverToken)
	{
		this.receiverToken = receiverToken;
	}

	public void setExpirationTime(Integer expirationTime)
	{
		this.expirationMinutes = expirationTime;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	public void setJsonWebKey( String key, String keyId ) {
	    try {
	        jsonWebKey = PublicJsonWebKey.Factory.newPublicJwk(key);
	        jsonWebKey.setKeyId(keyId);
	    } catch (JoseException e) {
	        e.printStackTrace();
	    }
	}

	public String produce( Map<String, Object> params) {
		PublicJsonWebKey rsaJsonWebKey = jsonWebKey;

		JwtClaims claims = new JwtClaims();
		claims.setIssuer(creatorToken);
		claims.setAudience(receiverToken);
		claims.setGeneratedJwtId();
		claims.setIssuedAtToNow();
		claims.setSubject(subject);

		if(expirationMinutes != null){
			claims.setExpirationTimeMinutesInTheFuture(expirationMinutes);
		}

		for (Map.Entry<String, Object> entry : params.entrySet()) {
			claims.setClaim(entry.getKey(),entry.getValue());
		}

		JsonWebSignature jws = new JsonWebSignature();
		jws.setPayload(claims.toJson());
		jws.setKey(rsaJsonWebKey.getPrivateKey());
		jws.setKeyIdHeaderValue(rsaJsonWebKey.getKeyId());
		jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

		String jwt = null;
		try {
			jwt = jws.getCompactSerialization();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		return jwt;
	}
}
