/**
 * 
 */
package generateKeyJWK.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class ValorStringUtil {

	public String retirarAspas(String jsonString) {

		String newJson = StringUtils.removeStart(jsonString, "\"");
		String jsonReturn = StringUtils.removeEnd(newJson, "\"");

		return jsonReturn;
	}

}
