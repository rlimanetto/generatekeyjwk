/**
 * 
 */
package generateKeyJWK.service;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;

import generateKeyJWK.request.GenerateKeyJWKRequest;
import generateKeyJWK.request.GenerateTokenRequest;
import generateKeyJWK.response.GenerateKeyJWKResponse;
import generateKeyJWK.response.GenerateTokenResponse;
import generateKeyJWK.util.JWTHelper;
import generateKeyJWK.util.ValorStringUtil;

@Service
public class GenerateKeyJWKService {

	private static final Logger Logger = LoggerFactory.getLogger(GenerateKeyJWKService.class.getName());
	
	@Autowired
	private ValorStringUtil valorStringUtil;

	private static final String SUCESS_CODE = "200";
	private static final String ERROR_CODE = "500";
	private static final String NO_RESULTS_CODE = "999";

	public GenerateKeyJWKResponse gerarChave(GenerateKeyJWKRequest generateKeyJWKRequest) {

		GenerateKeyJWKResponse jwkResponse = new GenerateKeyJWKResponse();

		try {
			if (StringUtils.isNotBlank(generateKeyJWKRequest.getIdChave())
					&& generateKeyJWKRequest.getTamanhoChave() != null 
					&& generateKeyJWKRequest.getTamanhoChave() > 0
					&& StringUtils.isNotBlank(generateKeyJWKRequest.getTipoChave())) {
												
				jwkResponse.setChave(keyBuilder(generateKeyJWKRequest).toJSONString());

				if (jwkResponse.getChave().isEmpty()) {
					jwkResponse.setStatusCode(NO_RESULTS_CODE);
				} else {
									
					jwkResponse.setStatusCode(SUCESS_CODE);

				}

			}
		} catch (NoSuchAlgorithmException e) {
			jwkResponse.setStatusCode(ERROR_CODE);
			Logger.error("Erro na geração da chave: " + e);
		}

		return jwkResponse;

	}
	
	public GenerateKeyJWKResponse getStatus() {

		GenerateKeyJWKResponse jwkResponse = new GenerateKeyJWKResponse();

		jwkResponse.setStatusCode(SUCESS_CODE);

		return jwkResponse;

	}
	
	/**
	 * Método para geração do token
	 * 
	 * @param key Chave de criptografia
	 * @param keyId Id da chave de criptografia
	 * @param creatorToken Sistema criador do token
	 * @param receiver Sistema destinatário do token
	 * @param subject Assunto sobre o que está criptografado no token
	 * @param sessionTimeout Tempo de validação do token em minutos
	 * @param params Map<String, Object> com os dados que serão criptografados no token
	 * @return String token criptografado
	 */
	public GenerateTokenResponse generateToken( GenerateTokenRequest generateTokenRequest) {

		JWTHelper jwtHelper = new JWTHelper();
		jwtHelper.setJsonWebKey(generateTokenRequest.getKey(), generateTokenRequest.getKeyId());
		jwtHelper.setCreatorToken(generateTokenRequest.getCreatorToken());
		jwtHelper.setReceiverToken(generateTokenRequest.getReceiver());
		jwtHelper.setSubject(generateTokenRequest.getSubject());
		jwtHelper.setExpirationTime(generateTokenRequest.getSessionTimeout());

		GenerateTokenResponse generateTokenResponse = new GenerateTokenResponse();
		
		String token = jwtHelper.produce(generateTokenRequest.getParams());
		
		if(StringUtils.isNotBlank(token)){
			generateTokenResponse.setStatusCode(SUCESS_CODE);
			generateTokenResponse.setToken(token);
		}else{
			generateTokenResponse.setStatusCode(ERROR_CODE);
			generateTokenResponse.setToken("");
		}
		
		return generateTokenResponse;

	}

	private JWK keyBuilder(GenerateKeyJWKRequest generateKeyJWKRequest) throws NoSuchAlgorithmException {
		// Generate the RSA key pair
		KeyPairGenerator gen = KeyPairGenerator.getInstance(generateKeyJWKRequest.getTipoChave());
		gen.initialize(generateKeyJWKRequest.getTamanhoChave()); // Set the desired key length
		KeyPair keyPair = gen.generateKeyPair();

		// Convert to JWK format
		JWK jwk = new RSAKey.Builder((RSAPublicKey)keyPair.getPublic())
		    .privateKey((RSAPrivateKey)keyPair.getPrivate())
		    .keyID(generateKeyJWKRequest.getIdChave()) // Give the key some ID (optional)
		    .build();
		
		return jwk;
	}

	private GenerateKeyJWKResponse parseValorStringJSON(String jsonRetorno) {

		GenerateKeyJWKResponse tableData = new GenerateKeyJWKResponse();
		try {
			String jsonRetornoFormatado = valorStringUtil.retirarAspas(jsonRetorno);
			tableData = new ObjectMapper().readValue(jsonRetornoFormatado, GenerateKeyJWKResponse.class);
		} catch (Exception e) {
			Logger.error("Erro no parse do JSON: " + e);
		}
		return tableData;
	}
}
